# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('architecture', '0012_auto_20151213_1740'),
    ]

    operations = [
        migrations.AlterField(
            model_name='post',
            name='nested_level',
            field=models.IntegerField(default=0),
        ),
    ]
