# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('architecture', '0010_auto_20151212_1830'),
    ]

    operations = [
        migrations.RenameField(
            model_name='thread',
            old_name='original_post',
            new_name='discussion',
        ),
        migrations.AlterField(
            model_name='discussion',
            name='start_date',
            field=models.DateTimeField(default=django.utils.timezone.now),
        ),
    ]
