# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('architecture', '0004_book_clubbook'),
    ]

    operations = [
        migrations.AlterField(
            model_name='club',
            name='members',
            field=models.ManyToManyField(related_name='subscribed_clubs', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AlterField(
            model_name='club',
            name='president',
            field=models.ForeignKey(related_name='owned_clubs', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AlterField(
            model_name='clubbook',
            name='book',
            field=models.ForeignKey(to='architecture.Book'),
        ),
        migrations.AlterField(
            model_name='clubbook',
            name='club',
            field=models.ForeignKey(to='architecture.Club'),
        ),
    ]
