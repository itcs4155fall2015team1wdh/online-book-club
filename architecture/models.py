from django.db import models
from django.contrib.auth.models import User
from django.utils import timezone

from django.conf import settings
from django.db.models.signals import post_save
from django.dispatch import receiver
from rest_framework.authtoken.models import Token
from descriptors import AutoOneToOneField

class UserDetail(models.Model):
    user = AutoOneToOneField(User, related_name='user_detail')
    password_reset_token = models.CharField(max_length=100, default='')

# Create your models here.
class Club(models.Model):
    president = models.ForeignKey(User, related_name='owned_clubs')
    name = models.CharField(max_length=30)
    description = models.TextField(max_length=320)
    members = models.ManyToManyField(User, related_name='subscribed_clubs', through='ClubMembership')
    creation_date = models.DateTimeField(auto_now_add=True)
    
    def __unicode__(self):
        return u'%s' % (self.name)

class ClubMembership(models.Model):
    club = models.ForeignKey(Club)
    user = models.ForeignKey(User)

    class Meta:
        unique_together = ('club', 'user')

class Book(models.Model):
    name = models.CharField(max_length=100)
    description = models.CharField(max_length=1000)
    author = models.CharField(max_length=80)
    def __unicode__(self):
        return u'%s' % (self.name)
    
class ClubBook(models.Model):
    book = models.ForeignKey(Book, related_name='club_books')
    start_date = models.DateTimeField(auto_now_add=True)
    end_date = models.DateTimeField()
    club = models.ForeignKey(Club, related_name='club_books')
    def __unicode__(self):
        return u'%s' % (self.book.name)

class Discussion(models.Model):
    club_book = models.ForeignKey(ClubBook, related_name='discussions')
    index = models.IntegerField()
    start_date = models.DateTimeField(default=timezone.now)

class Thread(models.Model):
    discussion = models.ForeignKey(Discussion, related_name='threads')

    def _get_original_post(self):
        for post in self.posts.all():
            if post.parent_post is None:
                return post
        return None

    original_post = property(_get_original_post)

class Post(models.Model):
    thread = models.ForeignKey(Thread, related_name='posts')
    parent_post = models.ForeignKey('self', related_name='child_posts', null=True, default=None)
    content = models.TextField(max_length=2000)
    nested_level = models.IntegerField(default=0)

