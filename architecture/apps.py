from django.apps import AppConfig

class ArchitectureAppConfig(AppConfig):
    name = 'architecture'
    verbose_name = 'Group Management'