====ITCS 4155 - Team 1====
======== Booklava ========

Elliot Simpson              -  Jsimps72@uncc.edu
Noah Potter                 -  jpotte19@uncc.edu
Hip Hop Legend Dylan Bailey -  dbaile30@uncc.edu
Walter "Durham" Harmon      -  wharmon1@uncc.edu

Make sure softwares are installed:
    python
    pip
    virtualenv
    postgresql (perferrably with the graphical installer) http://www.postgresql.org/download/

How to configure environment variables (Windows) -
    1) Find 'Advanced system settings' on your computer
    2) Click environment variables
    3) Append ';C:\python27;C:\python27\scripts' to the Path variable.
    4) If you are using cmd, restart it.

How to create the Python virtual environment:
    > cd root/booklava/folder
    > virtualenv env

How to activate the Python virtual environment:
    Linux/Mac:
        > cd root/booklava/folder
        > source env/bin/activate
        > deactivate

    Windows:
        > cd root/booklava/folder
        > ./Scripts/activate
        > deactivate

How to set up the database:
    Start the postgres server using: http://www.postgresql.org/docs/9.1/static/server-start.html
    > createuser -U postgres booklava -d -P
    > booklava
    > booklava
    > <your_password_for_postgres>
    Enter psql as postgres user
        > psql -U postgres
    Create the database for the app
        > CREATE DATABASE booklava_dev WITH OWNER=booklava;

    **Remember to migrate the database, detailed later**

############################################################
From here on it's assumed you have activated the virtualenv
############################################################

How to migrate the database
    > python manage.py migrate

How to install the libraries:
    > pip install -r requirements.txt

How to view site -
    > cd root/booklava/folder
    > python manage.py runserver <port [default=8000]>
    In your browser, navigate to 'http://127.0.0.1:<port number>'

How to run tests -
    > python manage.py test # all tests
    > python manage.py test tests.test_.UserTestCase.test_create_user_success # specific test

How to show routes - 
    > python manage.py show_urls

How to load data from scratch -
    > python manage.py loaddata data.json

How to interact with database manually:
    > python manage.py shell

    Example usage:
        > from architecture.models import Club
        > print Club.objects.all()

FIX (Make sure Python 2.7 is installed on your computer):
    1) In the PATH system variables, add an entry to \Scripts;
    2) If you're feeling like a daredevil, try removing the other PATH directories to python.
    3) If all else fails, run '.\Scripts\python manage.py runserver' with the latest commit, and you should be good to go.

/admin Account - 
Username: admin
Password: booklava01