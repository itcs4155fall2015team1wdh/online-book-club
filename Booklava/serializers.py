from django.contrib.auth.models import User
from architecture.models import (
    Club,
    ClubMembership,
    Book,
    ClubBook,
    Discussion,
    Thread,
    Post
)
from rest_framework import serializers, exceptions
from rest_framework.validators import UniqueValidator, UniqueTogetherValidator
from django.contrib.auth.hashers import make_password

from django.contrib.auth import authenticate
from django.utils.translation import ugettext_lazy as _

from django.core.exceptions import ObjectDoesNotExist

class UserSerializer(serializers.ModelSerializer):
    email = serializers.EmailField(validators=[UniqueValidator(queryset=User.objects.all())])
    password = serializers.CharField(write_only=True, required=False, min_length=5, error_messages={
            "blank": "Password cannot be empty.",
            "min_length": "Password too short.",
        })

    class Meta:
        model = User
        fields = ('id', 'url', 'username', 'email', 'password', 'owned_clubs', 'subscribed_clubs')
        read_only_fields = ('is_staff', 'owned_clubs', 'subscribed_clubs')

    def create(self, validated_data):
        user = User(
            email=validated_data['email'],
            username=validated_data['username'],
            password=validated_data['password']
        )
        # user.set_password(validated_data['password'])
        user.save()
        return user

    def validate_password(self, value):
        return make_password(value)

class BookSerializer(serializers.ModelSerializer):
    class Meta:
        model = Book
        fields = ('id', 'name', 'url', 'description', 'author')

class ClubSerializer(serializers.ModelSerializer):
    president = UserSerializer(read_only=True)
    members = UserSerializer(many=True, read_only=True)
    description = serializers.CharField(allow_blank=True)

    class Meta:
        model = Club
        fields = ('id', 'url', 'name', 'president', 'members', 'creation_date', 'description')

    def create(self, validated_data):
        user = None
        if hasattr(self.context['request'], 'user'):
            user = self.context['request'].user
        else:
            user = self.context['request']['user']
        club = Club(
            name=validated_data['name'],
            description=validated_data['description'],
            president=user
        )
        club.save()
        ClubMembership.objects.create(club=club, user=user)
        return club

class ClubMembershipSerializer(serializers.ModelSerializer):
    club = ClubSerializer(read_only=True, default=None)
    user = UserSerializer(read_only=True, default=None)
    club_id = serializers.PrimaryKeyRelatedField(queryset=Club.objects.all(), source='club', write_only=True)
    user_id = serializers.PrimaryKeyRelatedField(queryset=User.objects.all(), source='user', write_only=True)

    class Meta:
        model = ClubMembership
        fields = ('id', 'club', 'user', 'club_id', 'user_id')

class ClubBookSerializer(serializers.ModelSerializer):
    club = ClubSerializer(read_only=True)
    book = BookSerializer(read_only=True)
    club_id = serializers.PrimaryKeyRelatedField(queryset=Club.objects.all(), source='club', write_only=True)
    book_id = serializers.PrimaryKeyRelatedField(queryset=Book.objects.all(), source='book', write_only=True)
    
    class Meta:
        model = ClubBook
        fields = ('id', 'book', 'url', 'start_date','end_date','club', 'club_id', 'book_id')

class DiscussionSerializer(serializers.ModelSerializer):
    club_book = ClubBookSerializer(read_only=True)
    club_book_id = serializers.PrimaryKeyRelatedField(queryset=ClubBook.objects.all(), source='club_book', write_only=True)
    index = serializers.IntegerField(required=False, default=1)
    class Meta:
        model = Discussion
        fields = ('id', 'club_book', 'club_book_id', 'url', 'index', 'start_date')

    def validate_index(self, value):
        club_book = ClubBook.objects.get(pk=self.initial_data['club_book_id'])
        return club_book.discussions.count() + 1
        
class PostSerializer(serializers.ModelSerializer):
    thread = serializers.PrimaryKeyRelatedField(read_only=True)
    thread_id = serializers.PrimaryKeyRelatedField(queryset=Thread.objects.all(), source='thread', write_only=True)
    parent_post = serializers.PrimaryKeyRelatedField(read_only=True)
    parent_post_id = serializers.PrimaryKeyRelatedField(queryset=Post.objects.all(), source='parent_post', write_only=True, required=False, default=None)
    nested_level = serializers.IntegerField(read_only=True, default=0)

    class Meta:
        model = Post
        fields = ('id', 'thread', 'thread_id', 'url', 'parent_post', 'parent_post_id', 'content', 'nested_level')

    def validate_nested_level(self, value):
        if 'parent_post_id' in self.initial_data:
            parent_post = Post.objects.get(pk=self.initial_data['parent_post_id'])
            return parent_post.nested_level + 1
        else:
            return 0

class ThreadSerializer(serializers.ModelSerializer):
    discussion = DiscussionSerializer(read_only=True)
    discussion_id = serializers.PrimaryKeyRelatedField(queryset=Discussion.objects.all(), source='discussion', write_only=True)
    content = serializers.CharField(write_only=True, required=False)
    original_post = PostSerializer(read_only=True)

    class Meta:
        model = Thread
        fields = ('id', 'discussion', 'discussion_id', 'original_post', 'content', 'url')

    def create(self, validated_data):
        if 'content' in validated_data:
            thread = Thread(discussion=validated_data['discussion'])
            thread.save()
            post = thread.posts.create(content=validated_data['content'], nested_level=0)
            post.save()
            return thread
        else:
            thread = Thread(**validated_data)
            thread.save()
            return thread

# Pulled from the github page in order to allow logging in with email
class AuthTokenSerializer(serializers.Serializer):
    email = serializers.CharField()
    password = serializers.CharField(style={'input_type': 'password'})

    def validate(self, attrs):
        email = attrs.get('email')
        password = attrs.get('password')

        if email and password:
            user = None
            try:
                user = User.objects.get(email=email)
            except ObjectDoesNotExist:
                msg = _('Email invalid')
                raise exceptions.ValidationError(msg)

            if user:
                authenticated_user = authenticate(username=user.username, password=password)

                if authenticated_user:
                    if not authenticated_user.is_active:
                        msg = _('User account is disabled.')
                        raise exceptions.ValidationError(msg)
                else:
                    msg = _('Unable to log in with provided credentials.')
                    raise exceptions.ValidationError(msg)
        else:
            msg = _('Must include "email" and "password".')
            raise exceptions.ValidationError(msg)

        attrs['user'] = user
        return attrs