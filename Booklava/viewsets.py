from django.contrib.auth.models import User
from django.db.models import Q
from architecture.models import (
    Club,
    ClubMembership,
    Book,
    ClubBook,
    Discussion,
    Thread,
    Post,
    UserDetail
)
from django.contrib.auth.tokens import PasswordResetTokenGenerator
from rest_framework import (viewsets, status)
from rest_framework.authtoken import views
from rest_framework.response import Response
from rest_framework.decorators import detail_route, list_route
from rest_framework.authtoken.models import Token
from rest_framework.mixins import ListModelMixin
from rest_framework_extensions.mixins import NestedViewSetMixin
from rest_framework_extensions.utils import compose_parent_pk_kwarg_name
from django.core.exceptions import ObjectDoesNotExist
from Booklava.serializers import (
    UserSerializer,
    ClubMembershipSerializer,
    BookSerializer,
    ClubSerializer,
    DiscussionSerializer,
    ClubBookSerializer,
    ThreadSerializer,
    PostSerializer,
    AuthTokenSerializer
)

from django.core.mail import EmailMultiAlternatives
from django.template.loader import get_template

class SearchViewSet(ListModelMixin, viewsets.GenericViewSet):
    search_serializers = {
        'books': BookSerializer,
        'users': UserSerializer,
        'clubs': ClubSerializer
    }

    def get_queryset(self):
        query_type = self.request.query_params.get('type')
        query = self.request.query_params.get('query', None)
        result = Q()

        if query is not None:
            if query_type == 'clubs':
                queryset = Club.objects.all()
                for part in query.split(' '):
                    result = result | Q(name__icontains=part) | Q(description__icontains=part)

                return queryset.filter(result)
            elif query_type == 'users':
                queryset = User.objects.all()
                for part in query.split(' '):
                    result = result | Q(username__icontains=part)

                return queryset.filter(result)
            elif query_type == 'books':
                queryset = Book.objects.all()
                for part in query.split(' '):
                    result = result | Q(name__icontains=part)
                return queryset.filter(result)

        return None

    def get_serializer_class(self):
        query_type = self.request.query_params.get('type')
        return self.search_serializers[query_type]

class UserViewSet(NestedViewSetMixin, viewsets.ModelViewSet):
    queryset = User.objects.all()
    serializer_class = UserSerializer

class PasswordViewSet(viewsets.GenericViewSet):
    serializer_class = UserSerializer

    @list_route(methods=['post'])
    def forgot(self, request):
        email = request.data.get('email')
        try:
            user = User.objects.get(email=email)
        except ObjectDoesNotExist:
            return Response({'error': 'Email does not exist.'}, status=status.HTTP_400_BAD_REQUEST) 

        token_generator = PasswordResetTokenGenerator()
        token = token_generator.make_token(user)
        user.user_detail.password_reset_token = token
        user.user_detail.save()
        
        plaintext = get_template('forgot_password.txt')
        htmltext = get_template('forgot_password.html')
        data = { 'url': self.request.data.get('domain') + '/passwords/reset?token=' + token }

        subject, from_email, to = 'Booklava - Reset Password', 'booklava@outlook.com', self.request.data.get('email')
        text_content = plaintext.render(data)
        html_content = htmltext.render(data)
        msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
        msg.attach_alternative(html_content, 'text/html')
        msg.send()

        return Response({'success': 'Email sent.'}, status=status.HTTP_201_CREATED)

    @list_route(methods=['post'])
    def reset(self, request):
        token = self.request.data.get('token')

        try:
            user = UserDetail.objects.get(password_reset_token=token).user
        except ObjectDoesNotExist:
            return Response({'error': 'Expired token.'}, status=status.HTTP_400_BAD_REQUEST) 

        serializer = self.serializer_class(user, data={'password': self.request.data.get('password')}, partial=True, context={'request': request})
        serializer.is_valid(raise_exception=True)
        serializer.save()
        serializer.instance.user_detail.password_reset_token = ''
        serializer.instance.user_detail.save()
        return Response(serializer.data)


class BookViewSet(NestedViewSetMixin, viewsets.ModelViewSet):
    queryset = Book.objects.all()
    serializer_class = BookSerializer

class ClubViewSet(NestedViewSetMixin, viewsets.ModelViewSet):
    queryset = Club.objects.all()
    serializer_class = ClubSerializer

class UserClubViewSet(ClubViewSet):
    serializer_class = ClubMembershipSerializer
    queryset =  ClubMembership.objects.all()

    def destroy(self, request, *args, **kwargs):
        user_pk = kwargs[compose_parent_pk_kwarg_name('user')]
        user = User.objects.get(pk=user_pk)
        club = Club.objects.get(pk=kwargs['pk'])
        ClubMembership.objects.get(user=user, club=club).delete()
        return Response()

    def create(self, request, *args, **kwargs):
        user_pk = kwargs[compose_parent_pk_kwarg_name('user')]
        user = User.objects.get(pk=user_pk)
        serializer = ClubMembershipSerializer(data={'user_id': user.pk, 'club_id': kwargs['pk']}, context={'request': request})
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(serializer.data)

# Club Book
class ClubBookViewSet(NestedViewSetMixin, viewsets.ModelViewSet):
    queryset = ClubBook.objects.all()
    serializer_class = ClubBookSerializer

class ClubClubBookViewSet(ClubBookViewSet):
    serializer_class = ClubBookSerializer

# Discussion    
class DiscussionViewSet(NestedViewSetMixin, viewsets.ModelViewSet):
    queryset = Discussion.objects.all()
    serializer_class = DiscussionSerializer

class ClubBookDiscussionViewSet(DiscussionViewSet):
    serializer_class = DiscussionSerializer

# Thread
class ThreadViewSet(NestedViewSetMixin, viewsets.ModelViewSet):
    queryset = Thread.objects.all()
    serializer_class = ThreadSerializer

class DiscussionThreadViewSet(ThreadViewSet):
    serializer_class = ThreadSerializer

# Post
class PostViewSet(NestedViewSetMixin, viewsets.ModelViewSet):
    queryset = Post.objects.all()
    serializer_class = PostSerializer

class ThreadPostViewSet(PostViewSet):
    serializer_class = PostSerializer

# Auth Token
class ObtainAuthToken(views.ObtainAuthToken):
    serializer_class = AuthTokenSerializer

    # Copied from https://github.com/tomchristie/django-rest-framework/blob/6284bceaaff0e53349131164ce5c16cda8deb715/rest_framework/authtoken/views.py
    # Wanted to add id field
    def post(self, request):
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        user = serializer.validated_data['user']
        token, created = Token.objects.get_or_create(user=user)
        return Response({'token': token.key, 'id': user.id})
    
    @list_route(methods=['delete'])
    def delete(self, request):
        Token.objects.filter(user=request.user).delete()
        return Response({'success': 'success'})