from django.conf.urls import include, url
from django.contrib import admin
from rest_framework_extensions.routers import ExtendedSimpleRouter
from Booklava.viewsets import (
    UserViewSet,
    PasswordViewSet,
    ClubViewSet,
    UserClubViewSet,
    BookViewSet,
    ClubBookViewSet,
    ClubClubBookViewSet,
    ClubBookDiscussionViewSet,
    DiscussionViewSet,
    DiscussionThreadViewSet,
    ThreadViewSet,
    ThreadPostViewSet,
    PostViewSet,
    SearchViewSet,
    ObtainAuthToken
)

router = ExtendedSimpleRouter()

(
    router
        .register(r'users', UserViewSet, base_name='user')
        .register(r'clubs',
            UserClubViewSet,
            base_name='users-club',
            parents_query_lookups=['user'])
)

(
    router
        .register(r'clubs', ClubViewSet, base_name='club')
        .register(r'club_books',
            ClubClubBookViewSet,
            base_name='clubs-club_book',
            parents_query_lookups=['club'])
)

router.register(r'books', BookViewSet, base_name='book')

(

    router
        .register(r'club_books', ClubBookViewSet, base_name='clubbook')
        .register(r'discussions',
            ClubBookDiscussionViewSet,
            base_name='club_book-discussion',
            parents_query_lookups=['club_book'])

)

(
    router
        .register(r'discussions', DiscussionViewSet, base_name='discussion')
        .register(r'threads',
            DiscussionThreadViewSet,
            base_name='discussion-thread',
            parents_query_lookups=['discussion'])
)

(
    router
        .register(r'threads', ThreadViewSet, base_name='thread')
        .register(r'posts',
            ThreadPostViewSet,
            base_name='thread-post',
            parents_query_lookups=['thread'])
)

router.register(r'posts', PostViewSet, base_name='post')

router.register(r'search', SearchViewSet, base_name='search')

urlpatterns = [
    url(r'^', include(router.urls)),
    url(r'^passwords/forgot/', PasswordViewSet.as_view({'post': 'forgot'})),
    url(r'^passwords/reset/', PasswordViewSet.as_view({'post': 'reset'})),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^api-token-auth/', ObtainAuthToken.as_view()),
]
