import factory
from architecture import models

class UserFactory(factory.Factory):
    class Meta:
        model = models.User

    email = factory.Sequence(lambda n: 'test{0}@test.com'.format(n))
    username = factory.Sequence(lambda n: 'test{0}'.format(n))
    password = factory.Sequence(lambda n: 'test{0}'.format(n))

class ClubFactory(factory.Factory):
    class Meta:
        model = models.Club

    name = 'club name'
    description = 'club description'

    president = factory.SubFactory(UserFactory)

class BookFactory(factory.Factory):
    class Meta:
        model = models.Book

    name = 'book name'
    description = 'book description'
    author = 'book author'
    
class ClubBookFactory(factory.Factory):
    class Meta:
        model = models.ClubBook

    book = factory.SubFactory(BookFactory)
    club = factory.SubFactory(ClubFactory)
    end_date = '2015-05-25T00:00'
    
class DiscussionFactory(factory.Factory):
    class Meta:
        model = models.Discussion
    
class ThreadFactory(factory.Factory):
    class Meta:
        model = models.Thread
    
class PostFactory(factory.Factory):
    class Meta:
        model = models.Post

    content = 'content'
    nested_level = 1

    