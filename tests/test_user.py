from rest_framework.test import APITestCase
from rest_framework import status
from django.contrib.auth.models import User
from architecture.models import Club
from tests.utils import TestHelpers
from django.contrib.auth.hashers import (check_password, make_password)

from django.core import mail
from django.test import TestCase

class EmailTest(APITestCase):
    def test_reset_password_email_has_correct_url(self):
        self.test_reset_token_generated()     
        user = User.objects.last()
        self.assertEqual(len(mail.outbox), 1)
        self.assertEqual(mail.outbox[0].subject, 'Booklava - Reset Password')
        self.assertIn('localhost/passwords/reset?token=%s' % user.user_detail.password_reset_token , mail.outbox[0].body)

    def test_reset_token_generated(self):
        user = TestHelpers.login_user(self)['user']
        self.assertEqual(user.user_detail.password_reset_token, '')
        response = self.client.post('/passwords/forgot/', {'email': user.email, 'domain': 'localhost'}, format='json')
        user.user_detail.refresh_from_db()
        self.assertNotEqual(user.user_detail.password_reset_token, '')

    def test_password_reset_success(self):
        password = 'testpassword'
        user = TestHelpers.login_user(self)['user']
        self.client.post('/passwords/forgot/', {'email': user.email, 'domain': 'localhost'}, format='json')
        user.user_detail.refresh_from_db()
        response = self.client.post('/passwords/reset/', {'token': user.user_detail.password_reset_token, 'password': password}, format='json')
        user.refresh_from_db()
        user.user_detail.refresh_from_db()
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertTrue(user.check_password(password))
        self.assertEqual(user.user_detail.password_reset_token, '')

    def test_token_invalid(self):
        password = 'testpassword'
        result = TestHelpers.login_user(self)
        user = result['user']
        self.client.post('/passwords/forgot/', {'email': user.email, 'domain': 'localhost'}, format='json')
        user.user_detail.refresh_from_db()
        response = self.client.post('/passwords/reset/', {'token': 'incorrecttoken', 'password': password}, format='json')
        user.refresh_from_db()
        user.user_detail.refresh_from_db()
        self.assertTrue(user.check_password(result['userOb'].password))
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_new_password_invalid(self):
        password = 'inv'
        result = TestHelpers.login_user(self)
        user = result['user']
        self.client.post('/forgot_password/', {'email': user.email, 'domain': 'localhost'}, format='json')
        user.user_detail.refresh_from_db()
        response = self.client.post('/passwords/reset/', {'token': user.user_detail.password_reset_token, 'password': password}, format='json')
        user.refresh_from_db()
        user.user_detail.refresh_from_db()
        self.assertTrue(user.check_password(result['userOb'].password))
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response.data['password'], ['Password too short.'])

class TestCases(APITestCase):
    def test_create_user_success(self):
        o = TestHelpers.post_create_user(self)
        response = o['response']
        userOb = o['user']
        user = User.objects.first()
        self.assertNotEqual(user.password, 'test')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(response.data['username'], userOb.username)
        self.assertEqual(response.data['subscribed_clubs'], [])
        self.assertFalse('password' in response.data)

    def test_create_user_fail(self):
        response = self.client.post('/users/', {'username': 'test', 'email': '', 'password': ''}, format='json')
        self.assertFalse('username' in response.data)
        self.assertTrue('email' in response.data)
        self.assertTrue('password' in response.data)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_get_user_with_owned_club(self):
        user = TestHelpers.login_user(self)['user']
        self.client.post('/clubs/', {'name': 'test', 'description': 'test description'}, format='json')
        club = Club.objects.last()
        response = self.client.get('/users/%s/' % str(user.id))
        self.assertEqual(response.data['owned_clubs'], [club.id])
        
    ### Need to update to use /users/id/club/id
    def test_get_user_with_subscribed_club_success(self):
        user_1 = TestHelpers.login_user(self)['user'] # Login the primary user
        user_2 = TestHelpers.post_create_user(self)['user'] # Create a secondary user
        club = TestHelpers.create_club({'president': user_2, 'name': 'name', 'description': 'description'}) # Give the secondary user a club
        self.client.post('/users/%s/clubs/%s/' % (user_1.id, club.id), format='json') # Subscribe the primary user to the club
        response = self.client.get('/users/%s/' % str(user_1.id))
        self.assertEqual(response.data['owned_clubs'], [])
        self.assertEqual(response.data['subscribed_clubs'], [club.id])

    def test_joining_a_club_twice_fail(self):
        user = TestHelpers.login_user(self)['user']
        self.client.post('/clubs/', {'name': 'test', 'description': ''}, format='json')
        club = Club.objects.last()
        response = self.client.post('/users/%s/clubs/%s/' % (user.id, club.id), format='json')
        self.assertEqual(response.data['non_field_errors'], ['The fields club, user must make a unique set.'])

    def test_create_user_with_nonunique_email_fail(self):
        TestHelpers.create_user({'username': 'user1', 'email': 'test@test.com', 'password': 'test0'})
        response = self.client.post('/users/', {'username': 'user2', 'email': 'test@test.com', 'password': 'test0'}, format='json')
        self.assertFalse('username' in response.data)
        self.assertFalse('password' in response.data)
        self.assertTrue('email' in response.data)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_login_success(self):
        response = TestHelpers.login_user(self)['response']
        self.assertTrue('token' in response.data)
        self.assertTrue('id' in response.data)

    def test_login_fail(self):
        TestHelpers.post_create_user(self)
        response = self.client.post('/api-token-auth/', {'email':'wrongemail@test.com', 'password': 'test'}, format='json')
        self.assertFalse('token' in response.data)

    def test_logout_success(self):
        TestHelpers.login_user(self)
        response = self.client.delete('/api-token-auth/', format='json')
        self.assertFalse('token' in response.data)
        self.assertTrue('success' in response.data)

    def test_logout_fail(self):
        TestHelpers.login_user(self)
        self.client.credentials(HTTP_AUTHORIZATION='Token 000')
        response = self.client.delete('/api-token-auth/', format='json')
        self.assertFalse('token' in response.data)
        self.assertEqual(response.data['detail'], 'Invalid token.')
# 