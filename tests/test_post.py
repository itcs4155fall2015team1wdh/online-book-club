from rest_framework.test import APITestCase
from architecture.models import Post
from rest_framework import status
from tests.utils import TestHelpers

class TestCases(APITestCase):
    def test_get_post_success(self):
        TestHelpers.login_user(self)
        thread = TestHelpers.create_thread()
        post = TestHelpers.create_post({'thread_id': thread.id})
        response = self.client.get('/posts/%s/' % post.id, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['id'], post.id)
        self.assertEqual(response.data['thread'], thread.id)

    def test_create_post_success(self):
        TestHelpers.login_user(self)
        thread = TestHelpers.create_thread()
        response = self.client.post('/posts/', {'thread_id': thread.id, 'content': 'content'}, format='json')
        post = Post.objects.last()
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(response.data['id'], post.id)

    def test_get_posts_success(self):
        TestHelpers.login_user(self)
        thread = TestHelpers.create_thread()
        post_1 = TestHelpers.create_post({'thread_id': thread.id})
        post_2 = TestHelpers.create_post()
        response = self.client.get('/threads/%s/posts/' % thread.id, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data[0]['id'], post_1.id)
        self.assertEqual(len(response.data), 1)
