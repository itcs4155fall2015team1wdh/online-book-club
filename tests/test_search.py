from rest_framework.test import APITestCase
from architecture.models import ClubMembership
from django.contrib.auth.models import User
from architecture.models import Club
from rest_framework import status
from tests.utils import TestHelpers

class TestCases(APITestCase):
    def test_search_users_exact_username_success(self):
        user = TestHelpers.login_user(self)['user']
        response = self.client.get('/search/', {'query': user.username, 'type': 'users'}, format='json')
        self.assertEqual(len(response.data), 1)

    def test_search_users_partial_username_success(self):
        user = TestHelpers.login_user(self)['user']
        response = self.client.get('/search/', {'query': user.username[0:3], 'type': 'users'}, format='json')
        self.assertEqual(len(response.data), 1)

    def test_search_users_no_match_failure(self):
        user = TestHelpers.login_user(self)['user']
        response = self.client.get('/search/', {'query': 'notfound', 'type': 'users'}, format='json')
        self.assertEqual(len(response.data), 0)

    def test_search_club_name_success(self):
        TestHelpers.login_user(self)
        self.client.post('/clubs/', {'name': 'test', 'description': 'description'}, format='json')
        self.client.post('/clubs/', {'name': 'test', 'description': 'description'}, format='json')
        self.client.post('/clubs/', {'name': 'club1', 'description': 'description'}, format='json')
        response = self.client.get('/search/', {'query': 'test', 'type': 'clubs'}, format='json')
        self.assertEqual(len(response.data), 2)

    def test_search_club_description_success(self):
        TestHelpers.login_user(self)
        self.client.post('/clubs/', {'name': 'club1', 'description': 'test description'}, format='json')
        self.client.post('/clubs/', {'name': 'club2', 'description': 'test description'}, format='json')
        self.client.post('/clubs/', {'name': 'club3', 'description': 'description'}, format='json')
        response = self.client.get('/search/', {'query': 'test', 'type': 'clubs'}, format='json')
        self.assertEqual(len(response.data), 2)

    def test_search_club_description_multiple_words_success(self):
        TestHelpers.login_user(self)
        self.client.post('/clubs/', {'name': 'club1', 'description': 'test1 description'}, format='json')
        self.client.post('/clubs/', {'name': 'club2', 'description': 'test2 description'}, format='json')
        response = self.client.get('/search/', {'query': 'test1 test2', 'type': 'clubs'}, format='json')
        self.assertEqual(len(response.data), 2)

    def test_search_book_name_success(self):
        TestHelpers.login_user(self)
        TestHelpers.create_book({'name': 'test'})
        TestHelpers.create_book({'name': 'book1'})
        response = self.client.get('/search/', {'query': 'test', 'type': 'books'}, format='json')
        self.assertEqual(len(response.data), 1)

    def test_search_book_name_insensitive_success(self):
        TestHelpers.login_user(self)
        TestHelpers.create_book({'name': 'test'})
        TestHelpers.create_book({'name': 'book1'})
        response = self.client.get('/search/', {'query': 'TEST', 'type': 'books'}, format='json')
        self.assertEqual(len(response.data), 1)

