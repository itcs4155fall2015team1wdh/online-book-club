from rest_framework.test import APITestCase
from architecture.models import (Thread, Post)
from rest_framework import status
from tests.utils import TestHelpers

class TestCases(APITestCase):
    def test_get_thread_success(self):
        TestHelpers.login_user(self)
        discussion = TestHelpers.create_discussion()
        thread = TestHelpers.create_thread({'discussion_id': discussion.id})
        response = self.client.get('/threads/%s/' % thread.id, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['id'], thread.id)

    def test_get_thread_returns_original_post_success(self):
        TestHelpers.login_user(self)
        discussion = TestHelpers.create_discussion()
        thread = TestHelpers.create_thread({'discussion_id': discussion.id, 'content': 'content'})
        post = thread.original_post
        response = self.client.get('/threads/%s/' % thread.id, format='json')
        self.assertEqual(response.data['original_post']['id'], post.id)

    def test_create_thread_success(self):
        TestHelpers.login_user(self)
        discussion = TestHelpers.create_discussion()
        response = self.client.post('/threads/', {'discussion_id': discussion.id}, format='json')
        thread = Thread.objects.last()
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(response.data['id'], thread.id)

    def test_create_thread_and_post_success(self):
        TestHelpers.login_user(self)
        discussion = TestHelpers.create_discussion()
        response = self.client.post('/threads/', {'discussion_id': discussion.id, 'content': 'custom content'}, format='json')
        thread = Thread.objects.last()
        post = Post.objects.last()
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(response.data['id'], thread.id)
        self.assertEqual(post.content, 'custom content')
        self.assertEqual(post.thread, thread)

    def test_get_threads_success(self):
        TestHelpers.login_user(self)
        discussion = TestHelpers.create_discussion()
        thread_1 = TestHelpers.create_thread({'discussion_id': discussion.id})
        thread_2 = TestHelpers.create_thread()
        response = self.client.get('/discussions/%s/threads/' % discussion.id, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data[0]['id'], thread_1.id)
        self.assertEqual(len(response.data), 1)

    # def test_thread_index_increments_success(self):
        # TestHelpers.login_user(self)
        # discussion = TestHelpers.create_discussion()
        # thread_1 = TestHelpers.create_thread({'discussion_id': discussion.id})
        # thread_2 = TestHelpers.create_thread({'discussion_id': discussion.id})
        # response = self.client.get('/discussions/%s/threads/' % discussion.id, format='json')
        # self.assertEqual(response.status_code, status.HTTP_200_OK)
        # self.assertEqual(response.data[0]['id'], thread_1.id)
        # self.assertEqual(response.data[0]['index'], 1)
        # self.assertEqual(response.data[1]['id'], thread_2.id)
        # self.assertEqual(response.data[1]['index'], 2)
        # self.assertEqual(len(response.data), 2)
