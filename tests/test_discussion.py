from rest_framework.test import APITestCase
from architecture.models import Discussion
from rest_framework import status
from tests.utils import TestHelpers

class TestCases(APITestCase):
    def test_get_discussion_success(self):
        TestHelpers.login_user(self)
        club_book = TestHelpers.create_club_book()
        discussion = TestHelpers.create_discussion({'club_book_id': club_book.id})
        response = self.client.get('/discussions/%s/' % discussion.id, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['id'], discussion.id)
        self.assertEqual(response.data['index'], 1)

    def test_create_discussion_success(self):
        TestHelpers.login_user(self)
        club_book = TestHelpers.create_club_book()
        response = self.client.post('/discussions/', {'club_book_id': club_book.id}, format='json')
        discussion = Discussion.objects.last()
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(response.data['id'], discussion.id)
        self.assertEqual(response.data['club_book']['id'], club_book.id)
        self.assertEqual(response.data['index'], 1)

    def test_get_discussions_success(self):
        TestHelpers.login_user(self)
        club_book = TestHelpers.create_club_book()
        discussion_1 = TestHelpers.create_discussion({'club_book_id': club_book.id})
        discussion_2 = TestHelpers.create_discussion()
        response = self.client.get('/club_books/%s/discussions/' % club_book.id, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data[0]['id'], discussion_1.id)
        self.assertEqual(len(response.data), 1)

    def test_discussion_index_increments_success(self):
        TestHelpers.login_user(self)
        club_book = TestHelpers.create_club_book()
        discussion_1 = TestHelpers.create_discussion({'club_book_id': club_book.id})
        discussion_2 = TestHelpers.create_discussion({'club_book_id': club_book.id})
        response = self.client.get('/club_books/%s/discussions/' % club_book.id, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data[0]['id'], discussion_1.id)
        self.assertEqual(response.data[0]['index'], 1)
        self.assertEqual(response.data[1]['id'], discussion_2.id)
        self.assertEqual(response.data[1]['index'], 2)
        self.assertEqual(len(response.data), 2)
