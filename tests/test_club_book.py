from rest_framework.test import APITestCase
from architecture.models import ClubMembership
from django.contrib.auth.models import User
from architecture.models import Club
from rest_framework import status
from tests.utils import TestHelpers
from Booklava.serializers import ClubMembershipSerializer

class TestCases(APITestCase):
    def test_president_create_club_book_success(self):
        userOb = TestHelpers.login_user(self)['user']
        user = User.objects.get(username=userOb.username)
        club = TestHelpers.create_club({'president': user})
        book = TestHelpers.create_book()
        response = self.client.post('/club_books/', {'book_id': book.id, 'club_id': club.id, 'end_date': '2015-05-25T00:00' }, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(response.data['book']['id'], book.id)
        self.assertEqual(response.data['club']['id'], club.id)

    def test_get_club_book_success(self):
        club_book = TestHelpers.create_club_book()
        response = self.client.get('/club_books/%s/' % (club_book.id), format='json')
        self.assertEqual(response.data['book']['id'], club_book.book.id)
        self.assertEqual(response.data['club']['id'], club_book.club.id)

    def test_non_president_create_club_book_failure(self):
        pass

    def test_get_club_books_success(self):
        userOb = TestHelpers.login_user(self)['user']
        club_book_1 = TestHelpers.create_club_book()
        club_book_2 = TestHelpers.create_club_book()
        response = self.client.get('/clubs/%s/club_books/' % club_book_1.club.id, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data[0]['id'], club_book_1.id)
        self.assertEqual(len(response.data), 1)

    def test_update_club_book_success(self):
        club_book = TestHelpers.create_club_book()
        response = self.client.patch('/club_books/%s/' % (club_book.id), {'start_date': '2016-05-25T00:00'}, format='json')
        self.assertEqual(response.data['book']['id'], club_book.book.id)
        self.assertEqual(response.data['club']['id'], club_book.club.id)