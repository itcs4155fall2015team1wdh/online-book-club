from tests.factories import (UserFactory, ClubFactory, BookFactory, ClubBookFactory, DiscussionFactory, ThreadFactory, PostFactory)
from django.contrib.auth.models import User
from architecture.models import Club
from Booklava.serializers import (ClubSerializer, UserSerializer, BookSerializer, ClubBookSerializer, DiscussionSerializer, ThreadSerializer, PostSerializer)
import factory

class TestHelpers():
    @staticmethod
    def post_create_user(self):
        user = UserFactory.stub()
        response = self.client.post('/users/', {'username': user.username, 'email': user.email, 'password': user.password}, format='json')
        return {'response': response, 'userOb': user, 'user': User.objects.last()}

    @staticmethod
    def login_user(self):
        result = TestHelpers.post_create_user(self)
        response = self.client.post('/api-token-auth/', {'email': result['userOb'].email, 'password': result['userOb'].password}, format='json')
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + response.data['token'])
        return {'response': response, 'userOb': result['userOb'], 'user': result['user']}

    @staticmethod
    def create_user(data=None):
        if data is None:
            data = UserFactory.stub().__dict__
        else:
            data = UserFactory.stub(**data).__dict__

        user = UserSerializer(data=data)
        user.is_valid(raise_exception=True)
        user.save()
        return user.instance

    @staticmethod
    def create_club(data=None):
        if data is None:
            data = ClubFactory.stub().__dict__
        else:
            data = ClubFactory.stub(**data).__dict__

        club = ClubSerializer(data=data, context={'request': {'user': data['president']}})
        club.is_valid(raise_exception=True)
        data['president'].save()
        club.save()
        return club.instance

    @staticmethod
    def create_book(data=None):
        if data is None:
            data = BookFactory.stub().__dict__
        else:
            data = BookFactory.stub(**data).__dict__

        book = BookSerializer(data=data)
        book.is_valid(raise_exception=True)
        book.save()
        return book.instance

    @staticmethod
    def create_club_book(data=None):
        if data is None:
            club = TestHelpers.create_club()
            book = TestHelpers.create_book()
            data = ClubBookFactory.stub().__dict__
            data['club_id'] = club.id
            data['book_id'] = book.id
        else:
            data = ClubFactory.stub(**data).__dict__
        
        club_book = ClubBookSerializer(data=data)
        club_book.is_valid(raise_exception=True)
        club_book.save()
        return club_book.instance

    @staticmethod
    def create_discussion(data=None):
        if data is None:
            data = DiscussionFactory.stub().__dict__
        else:
            data = DiscussionFactory.stub(**data).__dict__

        if 'club_book_id' not in data:
            club_book = TestHelpers.create_club_book()
            data['club_book_id'] = club_book.id

        discussion = DiscussionSerializer(data=data)
        discussion.is_valid(raise_exception=True)
        discussion.save()
        return discussion.instance

    @staticmethod
    def create_thread(data=None):
        if data is None:
            data = ThreadFactory.stub().__dict__
        else:
            data = ThreadFactory.stub(**data).__dict__

        if 'discussion_id' not in data:
            discussion = TestHelpers.create_discussion()
            data['discussion_id'] = discussion.id

        thread = ThreadSerializer(data=data)
        thread.is_valid(raise_exception=True)
        thread.save()
        return thread.instance

    @staticmethod
    def create_post(data=None):
        if data is None:
            data = PostFactory.stub().__dict__
        else:
            data = PostFactory.stub(**data).__dict__

        if 'thread_id' not in data:
            thread = TestHelpers.create_thread()
            data['thread_id'] = thread.id

        post = PostSerializer(data=data)
        post.is_valid(raise_exception=True)
        post.save()
        return post.instance

