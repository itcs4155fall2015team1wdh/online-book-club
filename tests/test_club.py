from rest_framework.test import APITestCase
from architecture.models import ClubMembership
from django.contrib.auth.models import User
from architecture.models import Club
from rest_framework import status
from tests.utils import TestHelpers
from Booklava.serializers import ClubMembershipSerializer

class TestCases(APITestCase):
    def test_create_club_success(self):
        userOb = TestHelpers.login_user(self)['user']
        user = User.objects.get(username=userOb.username)
        response = self.client.post('/clubs/', {'name': 'test', 'description': 'test description'}, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(response.data['name'], 'test')
        self.assertEqual(response.data['president']['username'], user.username)
        self.assertEqual(response.data['president']['id'], user.id)
        self.assertEqual(response.data['members'][0]['username'], user.username)
        self.assertEqual(response.data['members'][0]['id'], user.id)

    def test_create_club_fail(self):
        TestHelpers.login_user(self)
        response = self.client.post('/clubs/', {'name': ''}, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_get_clubs_success(self):
        user = TestHelpers.login_user(self)['user']
        # Create a different user's membership that shouldn't be included
        stranger_user = TestHelpers.create_user()
        stranger_club = TestHelpers.create_club({'president': stranger_user})
        # Create a club using route
        self.client.post('/clubs/', {'name': 'test', 'description': 'test description'}, format='json')
        response = self.client.get('/users/%s/clubs/' % user.id, format='json')
        self.assertEqual(len(response.data), 1)

    def test_remove_subscribed_club_success(self):
        club = TestHelpers.create_club()
        user = TestHelpers.login_user(self)['user'] # Second user
        self.client.post('/users/%s/clubs/%s/' % (user.id, club.id), format='json')
        self.assertEqual(user.subscribed_clubs.count(), 1)
        response = self.client.delete('/users/%s/clubs/%s/' % (user.id, club.id), format='json')
        self.assertEqual(user.subscribed_clubs.count(), 0)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_delete_owned_club_success(self):
        user = TestHelpers.login_user(self)['user']
        self.client.post('/clubs/', {'name': 'test1', 'description': 'test description'}, format='json')
        response = self.client.delete('/clubs/%s/' % Club.objects.last().id, format='json')
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

    def test_create_club_membership_success(self):
        club = TestHelpers.create_club()
        user = TestHelpers.login_user(self)['user']
        response = self.client.post('/users/%s/clubs/%s/' % (user.pk, club.pk), format='json')
        self.assertEqual(response.data['club']['id'], club.pk)
        self.assertEqual(response.data['user']['id'], user.pk)

    def test_create_club_membership_club_doesnt_exist_failure(self):
        user = TestHelpers.login_user(self)['user']
        response = self.client.post('/users/%s/clubs/%s/' % (user.id, 999999), format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_create_club_membership_already_president_failure(self):
        user = TestHelpers.login_user(self)['user']
        club = TestHelpers.create_club({'president': user})
        response = self.client.post('/users/%s/clubs/%s/' % (user.id, club.id), format='json')
        self.assertEqual(response.data['non_field_errors'], ['The fields club, user must make a unique set.'])

    def test_delete_owned_club_deletes_subscriptions_success(self):
        pass